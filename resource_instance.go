package main

import "strings"

// ResourceInstance represents an instance of a given resource in the TF state file
type ResourceInstance struct {
	Attributes instanceAttributes `json:"attributes"`
}

// instanceAttributes represents the map of attributes of the resource instance in the TF state file
type instanceAttributes struct {
	Hostname          string            `json:"name"`
	AccessIPv4Address string            `json:"access_ip_v4"`
	Metadata          map[string]string `json:"metadata"`
}

// GetHostVars returns a map variables relevant to Ansible
func (r *ResourceInstance) GetHostVars() map[string]string {
	hostVars := make(map[string]string)

	hostVars["ansible_host"] = r.getAccessIPAddress()
	hostVars["access_ip_v4"] = r.getAccessIPAddress()
	hostVars["name"] = r.GetHostname()
	hostVars["sectag"] = r.getSecTag()
	hostVars["datacenter"] = r.getDatacenter()
	hostVars["environment"] = r.getEnvironment()
	hostVars["os_type"] = r.getOSType()
	hostVars["server_role"] = r.getServerRole()

	return hostVars
}

// GetHostname returns the resource instance hostname
func (r *ResourceInstance) GetHostname() string {
	return r.Attributes.Hostname
}

// getAccessIPAddress returns the resource instance IP address in the access network (IPv4 only)
func (r *ResourceInstance) getAccessIPAddress() string {
	return r.Attributes.AccessIPv4Address
}

// GetInventoryGroups returns the instance inventory groups (metadata.groups) in a slice
func (r *ResourceInstance) GetInventoryGroups() []string {
	var commaSeparatedGroups = r.Attributes.Metadata["groups"]
	groups := strings.Split(commaSeparatedGroups, ",")

	groups = append(groups, r.getDatacenter())
	groups = append(groups, r.getEnvironment())
	groups = append(groups, "all_"+r.getOSType())

	return groups
}

// IsPartOfInventoryGroup returns true if the instance is part of the specified group, false otherwise
func (r *ResourceInstance) IsPartOfInventoryGroup(group string) bool {
	for _, instanceGroup := range r.GetInventoryGroups() {
		if group == instanceGroup {
			return true
		}
	}

	return false
}

// getEnvironment returns the environment metadata key (metadata.environment)
func (r *ResourceInstance) getEnvironment() string {
	return r.Attributes.Metadata["environment"]
}

// getDatacenter returns the datacenter metadata key (metadata.datacenter)
func (r *ResourceInstance) getDatacenter() string {
	return r.Attributes.Metadata["datacenter"]
}

// getDatacenter returns the sectag metadata key (metadata.sectag)
func (r *ResourceInstance) getSecTag() string {
	return r.Attributes.Metadata["sectag"]
}

// getDatacenter returns the os_type metadata key (metadata.os_type)
func (r *ResourceInstance) getOSType() string {
	return r.Attributes.Metadata["os_type"]
}

// getServerRole returns the os_type metadata key (metadata.os_type)
func (r *ResourceInstance) getServerRole() string {
	return r.Attributes.Metadata["server_role"]
}
