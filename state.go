package main

import "encoding/json"

// State represents the TF state
type State struct {
	Resources []Resource `json:"resources"`
}

// Resource represents a resource from the "resources" tag in the TF state
type Resource struct {
	Mode      string             `json:"mode"`
	Type      string             `json:"type"`
	Instances []ResourceInstance `json:"instances"`
}

func (s *State) readFromString(stateFileContent string) error {
	// parse into struct
	err := json.Unmarshal([]byte(stateFileContent), s)
	if err != nil {
		return err
	}

	return nil
}
