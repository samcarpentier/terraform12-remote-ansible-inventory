# terraform12-remote-ansible-inventory

This is a litthe Go app which generates a dynamic [Ansible][ans] inventory from a [Terraform][tf] remote state file. It is compatible with Terraform 0.12+.

The following remote state backends are supported:

* Artifactory
* S3
* Terraform Cloud

The following providers are supported:

* OpenStack

## License

Apache License 2.0

[ans]: https://www.ansible.com
[tf]:  https://www.terraform.io
