package main

import (
	"encoding/json"
	"fmt"
	"io"
)

// metadata represents the _meta tag in the JSON inventory, which contains Ansible hostvars for all servers
type metadata struct {
	HostVars map[string]interface{} `json:"hostvars"`
}

// allGroup represents the Ansible inventory group named "all", which has a special structured in the JSON inventory
type allGroup struct {
	Hosts []string               `json:"hosts"`
	Vars  map[string]interface{} `json:"vars"`
}

func buildInventoryHostVars(state *State) metadata {
	hostVars := make(map[string]interface{})

	for _, resource := range state.Resources {
		if resource.Mode == "managed" && resource.Type == "openstack_compute_instance_v2" && len(resource.Instances) > 0 {
			for _, instance := range resource.Instances {
				hostVars[instance.GetHostname()] = instance.GetHostVars()
			}
		}
	}

	return metadata{HostVars: hostVars}
}

func buildInventoryGroupsMap(state *State) map[string]interface{} {
	all := make([]string, 0)
	inventoryGroups := make(map[string][]string)

	for _, resource := range state.Resources {
		if resource.Mode == "managed" && resource.Type == "openstack_compute_instance_v2" && len(resource.Instances) > 0 {
			for _, instance := range resource.Instances {
				all = append(all, instance.GetHostname())

				for _, group := range instance.GetInventoryGroups() {
					// fmt.Printf("Adding server %s to group %s\n", instance.GetHostname(), group)
					if inventoryGroups[group] == nil {
						inventoryGroups[group] = make([]string, 0)
					}

					inventoryGroups[group] = append(inventoryGroups[group], instance.GetHostname())
				}
			}
		}
	}

	outputGroups := make(map[string]interface{})
	outputGroups["all"] = &allGroup{
		Hosts: all,
		Vars:  make(map[string]interface{}),
	}

	for group := range inventoryGroups {
		outputGroups[group] = inventoryGroups[group]
	}

	return outputGroups
}

func listAllInventoryGroups(state *State) []string {
	// Leveraging map keys uniqueness since there are
	// no native 'set' data structures in Go
	uniqueInventoryGroupsMap := make(map[string]bool)

	for _, resource := range state.Resources {
		if resource.Mode == "managed" && resource.Type == "openstack_compute_instance_v2" && len(resource.Instances) > 0 {
			for _, instance := range resource.Instances {
				for _, inventoryGroup := range instance.GetInventoryGroups() {
					uniqueInventoryGroupsMap[inventoryGroup] = true
				}
			}
		}
	}

	uniqueInventoryGroups := make([]string, len(uniqueInventoryGroupsMap))

	i := 0
	for group := range uniqueInventoryGroupsMap {
		uniqueInventoryGroups[i] = group
		i++
	}

	return uniqueInventoryGroups
}

func getJSONInventory(stdout io.Writer, stderr io.Writer, state *State) int {
	inventoryGroups := buildInventoryGroupsMap(state)
	hostvars := buildInventoryHostVars(state)
	inventoryGroups["_meta"] = hostvars

	return writeJSONOutput(stdout, stderr, inventoryGroups)
}

func getSingleInstanceAttributes(stdout io.Writer, stderr io.Writer, state *State, host string) int {
	attributes := make(map[string]string)

	for _, resource := range state.Resources {
		if resource.Mode == "managed" && resource.Type == "openstack_compute_instance_v2" && len(resource.Instances) > 0 {
			for _, instance := range resource.Instances {
				if instance.GetHostname() == host {
					attributes = instance.GetHostVars()
				}
			}
		}
	}

	return writeJSONOutput(stdout, stderr, attributes)
}

func writeJSONOutput(stdout io.Writer, stderr io.Writer, inventory interface{}) int {
	inventoryJSON, err := json.Marshal(inventory)
	if err != nil {
		fmt.Fprintf(stderr, "Error encoding JSON: %s\n", err)
		return 1
	}

	_, err = stdout.Write(inventoryJSON)
	if err != nil {
		fmt.Fprintf(stderr, "Error writing JSON: %s\n", err)
		return 1
	}

	return 0
}
