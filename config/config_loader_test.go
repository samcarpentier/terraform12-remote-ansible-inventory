package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
	statesource "gitlab.com/samcarpentier/terraform-state-source"
)

const backendKey = "terraform_artifactory_repository"

func TestParseYamlConfiguration(t *testing.T) {
	config, _ := ParseBackendsConfiguration("./fixtures")

	assert.Equal(t, len(config.RemoteStates), 1)
	assert.Equal(t, "artifactory", config.RemoteStates[backendKey].Backend)
	assert.Equal(t, "https://artifactory.example.com/terraform/", config.RemoteStates[backendKey].URL)
	assert.Equal(t, "production", config.RemoteStates[backendKey].Repository)
	assert.Contains(t, config.RemoteStates[backendKey].Paths, "network")
	assert.Equal(t, "user.name", config.RemoteStates[backendKey].Username)
	assert.Equal(t, "definitelyNotMyNT", config.RemoteStates[backendKey].Password)
}

func TestValidateValidBackendsConfiguration(t *testing.T) {
	backend := statesource.BackendConfig{
		Backend:    "not-empty",
		Password:   "not-empty",
		Repository: "not-empty",
		Paths:      []string{"not-empty"},
		URL:        "not-empty",
		Username:   "not-empty",
	}
	backendsConfig := &BackendsConfig{
		RemoteStates: map[string]statesource.BackendConfig{backendKey: backend},
	}

	isValid := ValidateBackendsConfiguration(backendsConfig)

	assert.True(t, isValid)
}

func TestValidateInvalidBackendsConfiguration(t *testing.T) {
	backend := statesource.BackendConfig{}
	backendsConfig := &BackendsConfig{
		RemoteStates: map[string]statesource.BackendConfig{backendKey: backend},
	}

	isValid := ValidateBackendsConfiguration(backendsConfig)

	assert.False(t, isValid)
}

func TestValidateEmptyBackendsConfiguration(t *testing.T) {
	backendsConfig := &BackendsConfig{
		RemoteStates: map[string]statesource.BackendConfig{},
	}

	isValid := ValidateBackendsConfiguration(backendsConfig)

	assert.False(t, isValid)
}
