package config

import (
	statesource "gitlab.com/samcarpentier/terraform-state-source"
)

// BackendsConfig contains the details of each Terraform remote state backend
type BackendsConfig struct {
	RemoteStates map[string]statesource.BackendConfig `yaml:"remote_states"`
}
