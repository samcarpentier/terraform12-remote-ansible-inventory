package config

import (
	"encoding/base64"
	"io/ioutil"
	"log"
	"os"
	"path"

	yaml "gopkg.in/yaml.v2"
)

const sourcesFilename = "sources.yml"
const secretsFilename = "secrets.yml"

// ParseBackendsConfiguration searches for sources.yml and secrets.yml in the given
// directory. It then parses them into a structure and returns this structure.
func ParseBackendsConfiguration(directory string) (*BackendsConfig, error) {
	// log.Printf("Config dir: %s", directory)
	sourcesConfig, err := parseConfigFromPath(directory, sourcesFilename)

	if err != nil {
		// log.Printf("Unable to fetch sources.yml: %v", err)
		return nil, err
	}

	// log.Printf("Successfully fetched sources.yml")

	fullBackendsConfig, _ := appendSecretsToConfig(directory, sourcesConfig)

	return fullBackendsConfig, nil
}

// ValidateBackendsConfiguration validates that all fields of a BackendConfig
// structure are not empty
func ValidateBackendsConfiguration(backendsConfig *BackendsConfig) bool {
	if len(backendsConfig.RemoteStates) == 0 {
		return false
	}

	for _, v := range backendsConfig.RemoteStates {
		if v.Backend == "" || v.Password == "" || v.Repository == "" || len(v.Paths) == 0 || v.URL == "" || v.Username == "" {
			return false
		}
	}

	return true
}

func parseConfigFromPath(directory string, filename string) (*BackendsConfig, error) {
	var backendsConfig BackendsConfig
	var filePath = path.Join(directory, filename)

	// log.Printf("Parsing YAML configuration file %s", filePath)

	isFilePresent, err := fileExists(filePath)

	if !isFilePresent {
		// log.Printf("No file named %s in directory %s: %v", sourcesFilename, directory, err)
		return nil, nil
	}

	yamlFile, err := ioutil.ReadFile(filePath)

	if err != nil {
		// log.Printf("Unable to open YAML configuration file: %v ", err)
		return nil, err
	}

	err = yaml.Unmarshal(yamlFile, &backendsConfig)

	if err != nil {
		log.Fatalf("Unable to parse YAML configuration file: %v", err)
		return nil, err
	}

	return &backendsConfig, nil
}

func appendSecretsToConfig(directory string, backendsConfig *BackendsConfig) (*BackendsConfig, error) {
	secretsConfig, _ := parseConfigFromPath(directory, secretsFilename)

	if secretsConfig == nil {
		// log.Printf("Unable to fetch secrets.yml. Using backends config as-is.")
		for k, v := range backendsConfig.RemoteStates {
			v.Username = os.Getenv("ARTIFACTORY_USERNAME")
			v.Password = decodePassword(os.Getenv("ARTIFACTORY_PASSWORD"))
			v.AccessKey = os.Getenv("S3_ACCESS_KEY")
			v.SecretKey = decodePassword(os.Getenv("S3_SECRET_KEY"))
			v.Token = decodePassword(os.Getenv("TF_CLOUD_TOKEN"))

			backendsConfig.RemoteStates[k] = v
		}

		return backendsConfig, nil
	}

	// log.Printf("Successfully fetched secrets.yml")

	for k, v := range backendsConfig.RemoteStates {
		var associatedSecrets = secretsConfig.RemoteStates[k]

		if &associatedSecrets != nil {
			v.Username = associatedSecrets.Username
			v.Password = decodePassword(associatedSecrets.Password)
			v.AccessKey = associatedSecrets.AccessKey
			v.SecretKey = decodePassword(associatedSecrets.SecretKey)
			v.Token = decodePassword(associatedSecrets.Token)
		}

		backendsConfig.RemoteStates[k] = v
	}

	return backendsConfig, nil
}

func decodePassword(encodedPassword string) string {
	decodedPasswordBytes, err := base64.StdEncoding.DecodeString(encodedPassword)

	if err != nil {
		// log.Println("Password is not base64, using plain text mode")
		return encodedPassword
	}

	return string(decodedPasswordBytes)
}

func fileExists(path string) (bool, error) {
	_, err := os.Stat(path)

	if err == nil {
		return true, nil
	}

	if os.IsNotExist(err) {
		return false, nil
	}

	return true, err
}
