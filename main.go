package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"regexp"

	config "./config"
	statesource "gitlab.com/samcarpentier/terraform-state-source"
)

var list = flag.Bool("list", false, "list mode")
var host = flag.String("host", "", "host mode")
var inventory = flag.Bool("inventory", false, "inventory mode")
var configDir = flag.String("config", "", "Relative path to the configuration and secrets files directory")

const terraformVersionPattern = "\"terraform_version\": \"0.12|3"

func main() {
	flag.Parse()

	if !*list && *host == "" && !*inventory {
		fmt.Fprint(os.Stderr, "Either --host or --list must be specified\n")
		os.Exit(1)
	}

	backendsConfig, err := config.ParseBackendsConfiguration(*configDir)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to load configuration files: %s\n", err)
		os.Exit(1)
	}

	mergedStates := acquireAndMergeRemoteStateFiles(backendsConfig)

	// --list
	if *list {
		os.Exit(getJSONInventory(os.Stdout, os.Stderr, &mergedStates))
	}

	// --host <host>
	if *host != "" {
		os.Exit(getSingleInstanceAttributes(os.Stdout, os.Stderr, &mergedStates, *host))
	}
}

func acquireAndMergeRemoteStateFiles(backendsConfig *config.BackendsConfig) State {
	var mergedStates State
	for key, backend := range backendsConfig.RemoteStates {
		remoteStateSource, err := statesource.SelectStateSource(&backend)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to select remote state source for %s: %s\n", key, err)
			os.Exit(1)
		}

		for _, path := range backend.Paths {
			stateFileContent := getStateFileContentFromRemoteBackend(remoteStateSource, key, path)

			var state State
			if err := json.Unmarshal([]byte(stateFileContent), &state); err != nil {
				fmt.Fprintf(os.Stderr, "Unable to unmashal remote state file %s: %s\n", key, err)
				os.Exit(1)
			}

			for _, resource := range state.Resources {
				mergedStates.Resources = append(mergedStates.Resources, resource)
			}
		}
	}

	return mergedStates
}

func getStateFileContentFromRemoteBackend(remoteStateSource statesource.Source, key string, path string) string {
	stateFileContent, err := remoteStateSource.GetRemoteState(path)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to download remote state file for %s: %s\n", key, err)
		os.Exit(1)
	}

	terraformVersionRegex, _ := regexp.Compile(terraformVersionPattern)
	if !terraformVersionRegex.MatchString(stateFileContent) {
		fmt.Fprintf(os.Stderr, "Unsupported Terraform version for: %s, The only supported version is 0.12.\n", key)
		os.Exit(1)
	}

	return stateFileContent
}
